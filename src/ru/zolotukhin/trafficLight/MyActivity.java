package ru.zolotukhin.trafficLight;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

public class MyActivity extends Activity {
    /**
     * Called when the activity is first created.
     */
    @Override

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);

    }

    public void onClickGreenButton(View view){
        LinearLayout linearLayout;
        linearLayout=(LinearLayout) findViewById(R.id.backgroundLayout);
        linearLayout.setBackgroundColor(getResources().getColor(R.color.greenSignal));
    }
    public void onClickYellowButton(View view){
        LinearLayout linearLayout;
        linearLayout=(LinearLayout) findViewById(R.id.backgroundLayout);
        linearLayout.setBackgroundColor(getResources().getColor(R.color.yellowSignal));

    }
    public void onClickRedButton(View view){
        LinearLayout linearLayout;
        linearLayout=(LinearLayout) findViewById(R.id.backgroundLayout);
        linearLayout.setBackgroundColor(getResources().getColor(R.color.redSignal));
    }
}
